let cart = 0;

function addToCart() {
    cart++;
    updateCartDisplay();
    localStorage.setItem("cartCount", cart.toString());
}

function showCart() {
    if (cart !== 0) {
        alert("Vielen Dank für Ihre Bestellung");
        cart = 0;
        localStorage.removeItem("cartCount");
        updateCartDisplay();
    }
}

function updateCartDisplay() {
    const cartElement = document.getElementById("cart");
    if (cartElement) {
        cartElement.textContent = cart;
    }
}

function loadCartCount() {
    const storedCartCount = localStorage.getItem("cartCount");
    if (storedCartCount) {
        cart = parseInt(storedCartCount, 10);
        updateCartDisplay();
    }
}

window.addEventListener("load", loadCartCount);