let salads = [
    {
        "name": "Green salad with tomatoe",
        "prize": "4$",
        "id": 1,
        "ingredients": [
            "Iceberg lettuce",
            "Tomatoes"
        ],
        "imageUrl": "https://farm6.staticflickr.com/5087/5358599242_7251dc7de4.jpg"
    },
    {
        "name": "Tomato salad with mozzarella",
        "prize": "5$",
        "id": 2,
        "ingredients": [
            "Tomato",
            "Mozzarella"
        ],
        "imageUrl": "https://farm4.staticflickr.com/3130/5862973974_c107ed81ea.jpg"
    },
    {
        "name": "Field salad with egg",
        "prize": "4$",
        "id": 3,
        "ingredients": [
            "Field salad",
            "Egg"
        ],
        "imageUrl": "https://farm9.staticflickr.com/8223/8372222471_662acd24f6.jpg"
    },
    {
        "name": "Rocket with parmesan",
        "prize": "5$",
        "id": 4,
        "ingredients": [
            "Rocket",
            "Parmesan"
        ],
        "imageUrl": "https://farm8.staticflickr.com/7017/6818343859_bb69394ff2.jpg"
    }
]

for (const salad of salads) {

    document.getElementById('saladList').innerHTML += `
    <!-- Salad ${salad.id} -->
    <div class="item">
    <img class="imageSmall" src="${salad.imageUrl}" alt="Bild konnte nicht geladen werden.">
    <div class="salatinfo">
        <h4 class="itemname">${salad.name}</h4>
        <p class="beschreibung">${salad.ingredients.join(', ')}</p>
    </div>
    <div class="salatdetails">
        <select>
            <option value="italianDressing">Italian Dressing</option>
            <option value="frenchDressing">French Dressing</option>
        </select>
        <div class="shopping">
            <p class="dressingpreis">${salad.prize}</p>
            <button class="button" onclick="addToCart()">
                <img class="shoppingcart" src="bilder/shoppingcart.png"
                    alt="Bild konnte nicht geladen werden." />
            </button>
        </div>
    </div>
</div>
`;
}