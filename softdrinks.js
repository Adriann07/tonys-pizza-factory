let softdrink = [
    {
        "name": "Coke",
        "prize": "2$",
        "id": 1,
        "imageUrl": "https://farm1.staticflickr.com/71/203324363_b448827eb0.jpg",
        "volume": "50cl"
    },
    {
        "name": "Fanta",
        "prize": "2$",
        "id": 2,
        "imageUrl": "https://farm1.staticflickr.com/684/32876893826_130576f75a.jpg",
        "volume": "50cl"
    },
    {
        "name": "Pepsi",
        "prize": "2$",
        "id": 3,
        "imageUrl": "https://farm4.staticflickr.com/3344/3593103557_bf47c0a3a2.jpg",
        "volume": "50cl"
    },
    {
        "name": "Red bull",
        "prize": "3$",
        "id": 4,
        "imageUrl": "https://farm3.staticflickr.com/2391/2507916617_254348d40c.jpg",
        "volume": "50cl"
    }
]

for (const softdrinks of softdrink) {

    document.getElementById('softdrinkslist').innerHTML += `
    <!-- Softdrink ${softdrinks.id} -->
    <div class="item">
        <img class="softImage" src="${softdrinks.imageUrl}" alt="Bild konnte nicht geladen werden." />
        <div class="itemName">
            <h4>${softdrinks.name}</h4>
        </div>
        <div class="softdrinkinfo">
            <select>
                <option value="33cl">33cl</option>
                <option value="50cl">50cl</option>
                <option value="1l">1l</option>
            </select>
            <div class="shopping">
                <p class="softDrinkPrice">${softdrinks.prize}</p>
                <button class="button" onclick="addToCart()">
                    <img class="shoppingcart" src="bilder/shoppingcart.png"
                        alt="Bild konnte nicht geladen werden." />
                </button>
            </div>
        </div>
    </div>
`;
}